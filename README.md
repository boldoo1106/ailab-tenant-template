# Ailab Template - Tenant

![N|Solid](https://www.invescore.mn/resource/img/logo.png)


### Postman Collection

- [Collection татах](https://www.getpostman.com/collections/fb9bc00132ceacba7e14)

### Swagger URL

- {server-host}:{server:port}/swagger-ui.html

### Хийгдэх зүйлс

- [✓] Олон шхем дуудах
- [✖] Unit test template нэмэх @boldoo1106
- [✓] Feature template flag нэмэх

### Tenant тохиргоо

**1. Ерөнхий бүтэц болон эхлэл**

```java
@Bean
public DataSource dataSource() {
    return new TenantRoutingSource("/tenants.json");
}
```

```json
[
    {
      "tenant" : "Tenant-Invescore-Central",
      "dataSourceClassName": "org.postgresql.ds.PGSimpleDataSource",
      "url": "jdbc:postgresql://127.0.0.1:5432/test-schema-1",
      "user": "tester-1",
      "password": "1234",
      "failTimeout": 0,
      "maxPoolSize": 5
    },
    {
      "tenant": "Tenant-Branch-1",
      "dataSourceClassName": "org.postgresql.ds.PGSimpleDataSource",
      "url": "jdbc:postgresql://127.0.0.1:5432/test-schema-2",
      "user": "tester-2",
      "password": "1234",
      "failTimeout": 0,
      "maxPoolSize": 5
    }
  ]
  ```
**2. Тенант солих** 
> **User || Client** - дээр салбар бүртгэлтэй гэж үзнэ

```java
TenantStorage.setTenantName(tenantName);
```

### Feature flags
1. application.properties
   ```properties
    feature.region=kg
    feature.modules.company=true
    feature.modules.company-version=v1
   ```
2. Controller дээр тухайн **feature** нээлттэй эсхийг шалгах

   ```java
    @RestController
    @RequestMapping("/api/tenant/company")
    @ConditionalOnExpression("${feature.modules.company} && {'${feature.region}' == 'kg'}")
    public class CompanyController {}
   ```
3. Service дээр **version** шалгаж ажиллуулах
   ```java
    @Service
    @ConditionalOnProperty(name = "feature.modules.company-version", havingValue = "v2")
    public class CompanyServiceV2 implements CompanyService {}
   ```



