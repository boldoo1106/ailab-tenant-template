package mn.ailab.company.repository;

import org.springframework.data.repository.CrudRepository;

import mn.ailab.company.model.Company;

public interface CompanyRepository extends CrudRepository<Company, Long> {}
