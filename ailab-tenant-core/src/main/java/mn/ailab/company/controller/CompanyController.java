package mn.ailab.company.controller;

import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import mn.ailab.company.exception.ResourceNotFoundException;
import mn.ailab.company.service.CompanyService;

@RestController
@RequestMapping("/api/tenant/company")
@ConditionalOnExpression("${feature.modules.company} && {'${feature.region}' == 'kg'}")
public class CompanyController {

  private final CompanyService companyService;

  public CompanyController(final CompanyService companyService) {
    this.companyService = companyService;
  }

  @PostMapping
  @ApiParam("Өөрийн бүртгэлтэй компаний мэдээллийг авах")
  @ApiOperation("Өөрийн бүртгэлтэй компаний мэдээллийг авах")
  public ResponseEntity<Object> getPassportInfo() {

    try {
      // Tenant-Kyrgyz
      return ResponseEntity.ok(companyService.getCurrentTenantInfo("Tenant-Invescore"));
    } catch (final Exception e) {
      throw new ResourceNotFoundException(e.getMessage());
    }
  }
}
