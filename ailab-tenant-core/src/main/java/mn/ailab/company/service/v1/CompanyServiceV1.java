package mn.ailab.company.service.v1;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import mn.ailab.company.repository.CompanyRepository;
import mn.ailab.company.service.CompanyService;
import mn.ailab.tenant.core.TenantStorage;

@Service
@ConditionalOnProperty(
    name = "feature.modules.company-version",
    havingValue = "v1",
    matchIfMissing = true)
public class CompanyServiceV1 implements CompanyService {

  private final CompanyRepository companyRepository;

  public CompanyServiceV1(final CompanyRepository companyRepository) {
    this.companyRepository = companyRepository;
  }

  @Override
  public Object getCurrentTenantInfo(final String tenantName) throws Exception {
    TenantStorage.setTenantName(tenantName);

    return companyRepository.findAll();
  }
}
