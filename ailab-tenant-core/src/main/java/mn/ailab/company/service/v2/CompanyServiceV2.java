package mn.ailab.company.service.v2;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import mn.ailab.company.service.CompanyService;
import mn.ailab.tenant.core.TenantStorage;

@Service
@ConditionalOnProperty(name = "feature.modules.company-version", havingValue = "v2")
public class CompanyServiceV2 implements CompanyService {

  @Override
  public Object getCurrentTenantInfo(final String tenantName) throws Exception {
    TenantStorage.setTenantName(tenantName);

    throw new UnsupportedOperationException("Хөгжүүлэлт хийгдэж байна");
  }
}
