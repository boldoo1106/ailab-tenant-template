package mn.ailab.company.service;

public interface CompanyService {

  public Object getCurrentTenantInfo(String tenantName) throws Exception;
}
