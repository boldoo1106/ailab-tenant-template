package mn.ailab.company.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -6080980343407378112L;

  public ResourceNotFoundException(final String exception) {
    super(exception);
  }
}
