package mn.ailab.company.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class TenantExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(Exception.class)
  public final ResponseEntity<Object> handleAllExceptions(
      final Exception ex, final WebRequest request) {

    final List<String> details = new ArrayList<>();
    details.add(ex.getLocalizedMessage());
    final var error = new ErrorResponse("Server Error", details);
    return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(ResourceNotFoundException.class)
  public final ResponseEntity<Object> handleRecordNotFoundException(
      final ResourceNotFoundException ex, final WebRequest request) {

    final List<String> details = new ArrayList<>();
    details.add(ex.getLocalizedMessage());
    final var error = new ErrorResponse("Өгөгдөл олдсонгүй", details);
    return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
  }
}
