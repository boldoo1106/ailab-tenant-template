package mn.ailab.company.exception;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorResponse {

  private final String message;
  private final List<String> details;

  public ErrorResponse(final String message, final List<String> details) {
    super();

    this.message = message;
    this.details = details;
  }
}
