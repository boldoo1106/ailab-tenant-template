package mn.ailab.tenant.core;

public class TenantStorage {

  private TenantStorage() {}

  private static ThreadLocal<String> tenant = new ThreadLocal<>();

  public static void setTenantName(final String name) {
    tenant.set(name);
  }

  public static String getTenantName() {
    return tenant.get();
  }
}
