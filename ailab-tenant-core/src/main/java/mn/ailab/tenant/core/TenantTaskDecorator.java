package mn.ailab.tenant.core;

import org.springframework.core.task.TaskDecorator;

public class TenantTaskDecorator implements TaskDecorator {

  @Override
  public Runnable decorate(final Runnable runnable) {
    final var tenantName = TenantStorage.getTenantName();

    return () -> {
      try {
        TenantStorage.setTenantName(tenantName);
        runnable.run();
      } finally {
        TenantStorage.setTenantName(null);
      }
    };
  }
}
