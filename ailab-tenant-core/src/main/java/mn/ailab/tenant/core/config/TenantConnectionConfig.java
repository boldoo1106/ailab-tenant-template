package mn.ailab.tenant.core.config;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TenantConnectionConfig {

  private final String tenant;
  private final String url;
  private final String user;
  private final String dataSourceClassName;
  private final String password;

  private final Integer failTimeout;
  private final Integer maxPoolSize;

  @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
  public TenantConnectionConfig(
      @JsonProperty("url") final String url,
      @JsonProperty("user") final String user,
      @JsonProperty("tenant") final String tenant,
      @JsonProperty("password") final String password,
      @JsonProperty("failTimeout") final Integer failTimeout,
      @JsonProperty("maxPoolSize") final Integer maxPoolSize,
      @JsonProperty("dataSourceClassName") final String dataSourceClassName) {

    this.url = url;
    this.user = user;
    this.tenant = tenant;
    this.password = password;
    this.maxPoolSize = maxPoolSize;
    this.failTimeout = failTimeout;
    this.dataSourceClassName = dataSourceClassName;
  }

  @JsonProperty("failTimeout")
  public Integer getFailTimeout() {
    return failTimeout;
  }

  @JsonProperty("maxPoolSize")
  public Integer getMaxPoolSize() {
    return maxPoolSize;
  }

  @JsonProperty("tenant")
  public String getTenant() {
    return tenant;
  }

  @JsonProperty("url")
  public String getUrl() {
    return url;
  }

  @JsonProperty("user")
  public String getUser() {
    return user;
  }

  @JsonProperty("dataSourceClassName")
  public String getDataSourceClassName() {
    return dataSourceClassName;
  }

  @JsonProperty("password")
  public String getPassword() {
    return password;
  }

  @Override
  public boolean equals(final Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    final var that = (TenantConnectionConfig) o;
    return Objects.equals(tenant, that.tenant)
        && Objects.equals(url, that.url)
        && Objects.equals(user, that.user)
        && Objects.equals(dataSourceClassName, that.dataSourceClassName)
        && Objects.equals(password, that.password);
  }

  @Override
  public int hashCode() {
    return Objects.hash(tenant, url, user, dataSourceClassName, password);
  }

  @Override
  public String toString() {
    return "TenantConnectionConfig [tenant="
        + tenant
        + ", url="
        + url
        + ", user="
        + user
        + ", dataSourceClassName="
        + dataSourceClassName
        + ", password="
        + password
        + ", failTimeout="
        + failTimeout
        + ", maxPoolSize="
        + maxPoolSize
        + "]";
  }
}
