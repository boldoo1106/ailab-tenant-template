package mn.ailab.tenant.core.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import mn.ailab.tenant.core.TenantStorage;

public class TenantAwareRoutingSource extends AbstractRoutingDataSource {

  @Override
  protected Object determineCurrentLookupKey() {
    return TenantStorage.getTenantName();
  }
}
