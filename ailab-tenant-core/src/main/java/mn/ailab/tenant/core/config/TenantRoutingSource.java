package mn.ailab.tenant.core.config;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
import org.springframework.scheduling.annotation.Scheduled;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariDataSource;

import mn.ailab.company.exception.ResourceNotFoundException;
import mn.ailab.tenant.core.TenantStorage;

public class TenantRoutingSource extends AbstractRoutingDataSource {

  private final String filename;
  private final ObjectMapper objectMapper;

  private final ConcurrentMap<String, HikariDataSource> tenantsDataSources;

  public TenantRoutingSource(final String filename) {
    this(filename, new ObjectMapper());
  }

  public TenantRoutingSource(final String filename, final ObjectMapper objectMapper) {
    this.filename = filename;
    this.objectMapper = objectMapper;

    this.tenantsDataSources = buildDataSources();
  }

  @Override
  public void afterPropertiesSet() {
    // Calls nothing.
  }

  @Override
  protected DataSource determineTargetDataSource() {
    final var lookupKey = (String) determineCurrentLookupKey();
    return tenantsDataSources.get(lookupKey);
  }

  @Override
  protected Object determineCurrentLookupKey() {
    return TenantStorage.getTenantName();
  }

  private ConcurrentMap<String, HikariDataSource> buildDataSources() {
    final var configurations = loadConnectionConfigs();

    return Arrays.stream(configurations)
        .collect(
            Collectors.toConcurrentMap(TenantConnectionConfig::getTenant, this::buildDataSource));
  }

  private TenantConnectionConfig[] loadConnectionConfigs() {
    try {
      final var url = this.getClass().getResource(filename);
      return objectMapper.readValue(new File(url.toURI()), TenantConnectionConfig[].class);
    } catch (final Exception e) {
      throw new ResourceNotFoundException(e.getMessage());
    }
  }

  private HikariDataSource buildDataSource(final TenantConnectionConfig configuration) {
    final var dataSource = new HikariDataSource();

    // Load from configurations?
    dataSource.setInitializationFailTimeout(configuration.getFailTimeout());
    dataSource.setMaximumPoolSize(configuration.getMaxPoolSize());

    dataSource.setDataSourceClassName(configuration.getDataSourceClassName());
    dataSource.addDataSourceProperty("url", configuration.getUrl());
    dataSource.addDataSourceProperty("user", configuration.getUser());
    dataSource.addDataSourceProperty("password", configuration.getPassword());

    return dataSource;
  }

  // CHECKME By seconds
  @Scheduled(fixedDelay = 2000)
  public void refreshDataSources() {
    final var configurations = loadConnectionConfigs();

    this.removeObsoleteTenants(configurations);
    this.updateTenants(configurations);
  }

  private void updateTenants(final TenantConnectionConfig[] configurations) {
    for (final TenantConnectionConfig configuration : configurations) {
      if (tenantsDataSources.containsKey(configuration.getTenant())) {
        final var dataSource = tenantsDataSources.get(configuration.getTenant());

        // update configuration string
        if (!isCurrentConfiguration(dataSource, configuration)) {
          dataSource.close();
          tenantsDataSources.put(configuration.getTenant(), buildDataSource(configuration));
        }
      } else {
        // insert configuration config string
        tenantsDataSources.put(configuration.getTenant(), buildDataSource(configuration));
      }
    }
  }

  private void removeObsoleteTenants(final TenantConnectionConfig[] configurations) {

    final var tenantNamesFromConfiguration =
        Arrays.stream(configurations)
            .map(TenantConnectionConfig::getTenant)
            .collect(Collectors.toSet());

    for (final String tenant : tenantsDataSources.keySet()) {

      if (!tenantNamesFromConfiguration.contains(tenant)) {
        final var dataSource = tenantsDataSources.get(tenant);
        dataSource.close();
        tenantsDataSources.remove(tenant);
      }
    }
  }

  private boolean isCurrentConfiguration(
      final HikariDataSource dataSource, final TenantConnectionConfig configuration) {
    return Objects.equals(
            dataSource.getDataSourceProperties().getProperty("user"), configuration.getUser())
        && Objects.equals(
            dataSource.getDataSourceProperties().getProperty("url"), configuration.getUrl())
        && Objects.equals(
            dataSource.getDataSourceProperties().getProperty("password"),
            configuration.getPassword())
        && Objects.equals(
            dataSource.getDataSourceClassName(), configuration.getDataSourceClassName());
  }
}
