package mn.ailab.config;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import mn.ailab.tenant.core.TenantTaskDecorator;

@Configuration
public class AsyncConfig extends AsyncConfigurerSupport {

  @Override
  public Executor getAsyncExecutor() {
    final var executor = new ThreadPoolTaskExecutor();

    executor.setCorePoolSize(7);
    executor.setMaxPoolSize(42);
    executor.setQueueCapacity(11);
    executor.setThreadNamePrefix("AilabTenantTaskExecutor ->");
    executor.setTaskDecorator(new TenantTaskDecorator());
    executor.initialize();

    return executor;
  }
}
