package mn.ailab.config;

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Bean
  public Docket swaggerLoanApi10() {
    return new Docket(DocumentationType.SWAGGER_2)
        .groupName("tenant-api-1.0")
        .select()
        .apis(RequestHandlerSelectors.basePackage("mn.ailab.company.controller"))
        .paths(regex("/api/.*"))
        .build()
        .useDefaultResponseMessages(false)
        .apiInfo(
            new ApiInfoBuilder()
                .version("1.0")
                .title("Tenant example API")
                .description("Салбар нэгж дээр тусдаа схем, өгөгдлийн бааз дуудах жишээ")
                .build());
  }
}
