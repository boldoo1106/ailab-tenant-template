package mn.ailab.feature;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ConfigurationProperties("feature")
public class FeatureProperties {

  private String region;
  private final Map<String, Object> modules = new HashMap<>();
}
