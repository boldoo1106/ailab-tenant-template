package mn.ailab.feature;

import java.util.Map;

import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

/**
 * Actuator feature uud harah zorilgoor - performance udaashruulah uchir actuator iig haaj bolno.
 */
@Component
@Endpoint(id = "modules")
public class FeatureToggleInfoEndpoint {

  private final FeatureProperties features;

  public FeatureToggleInfoEndpoint(final FeatureProperties features) {
    this.features = features;
  }

  @ReadOperation
  public Map<String, Object> featureToggles() {
    return features.getModules();
  }
}
