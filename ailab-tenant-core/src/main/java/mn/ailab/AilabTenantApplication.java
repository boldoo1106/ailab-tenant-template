package mn.ailab;

import javax.sql.DataSource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import mn.ailab.tenant.core.config.TenantRoutingSource;

@SpringBootApplication
@EnableAsync
@EnableScheduling
@EnableTransactionManagement
public class AilabTenantApplication {

  public static void main(final String[] args) {
    SpringApplication.run(AilabTenantApplication.class, args);
  }

  @Bean
  public DataSource dataSource() {
    return new TenantRoutingSource("/tenants.json");
  }
}
